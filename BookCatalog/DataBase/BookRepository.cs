﻿using Shared.DtoModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
	/// <summary>
	/// Repository for work with Book model
	/// </summary>
	public class BookRepository : Repository<Book>
	{
		public BookRepository(BookCatalogContext context) :
			base(context)
		{

		}
	}
}
