﻿using Microsoft.EntityFrameworkCore;
using Shared.DtoModels;

namespace DataBase
{
	/// <summary>
	/// Book Catalog context for work with DB
	/// </summary>
	public class BookCatalogContext: DbContext
	{
		public DbSet<Book> Books { get; set; }

		public BookCatalogContext(DbContextOptions options)
			  : base(options)
		{
		}
	}
}
