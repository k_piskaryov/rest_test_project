﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataBase;
using Microsoft.Extensions.Logging;
using Shared;
using Shared.DtoModels;
using Shared.Models;

namespace WebApi.Services
{
	public class BookService : IBookService
	{
		private readonly BookRepository _bookRepository;
		private readonly ILogger _logger;
		public BookService(BookRepository bookRepository, ILogger<BookService> logger)
		{
			_bookRepository = bookRepository;
			_logger = logger;
		}

		/// <summary>
		/// Create new Book in DB
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public long CreateBook(BookRequest data)
		{
			try
			{
				var book = GetBookByRequest(data);
				_bookRepository.Create(book);
				return book.Id;
			}
			catch(Exception ex)
			{
				_logger.LogWarning(ex, "CreateBook error");
				return 0;
			}
		}

		/// <summary>
		/// Delete Book by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public bool DeleteBook(long id)
		{
			try
			{
				var book = _bookRepository.GetById(id);
				if(book == null)
				{
					return false;
				}
				_bookRepository.Delete(book);
				return true;
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "DeleteBook error");
				return false;
			}
		}

		/// <summary>
		/// Get List Books
		/// </summary>
		/// <returns></returns>
		public IEnumerable<BookResponse> GetBooks()
		{
			var defaultResult = new List<BookResponse>();
			try
			{
				var books = _bookRepository.Get();
				defaultResult = books.Select(GetBookResponseByModel).ToList();
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "GetBooks error");
			}

			return defaultResult;
		}

		/// <summary>
		/// Get Book by Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public BookResponse GetBook(long id)
		{
			try
			{
				var book = _bookRepository.GetById(id);
				return GetBookResponseByModel(book);
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "GetBook error");
				return null;
			}
		}

		/// <summary>
		/// Update Book in DB
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public bool UpdateBook(BookRequest data)
		{
			try
			{
				var book = GetBookByRequest(data);
				_bookRepository.Update(book);
				return true;
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "UpdateBook error");
				return false;
			}
		}

		private Book GetBookByRequest(BookRequest data)
		{
			return new Book
			{
				Id = data.BookId ?? 0,
				Name = data.Name,
				Author = data.Author,
				Pages = data.Pages
			};
		}

		private BookResponse GetBookResponseByModel(Book data)
		{
			return new BookResponse
			{
				BookId = data.Id,
				Name = data.Name,
				Author = data.Author,
				Pages = data.Pages
			};
		}
	}
}
