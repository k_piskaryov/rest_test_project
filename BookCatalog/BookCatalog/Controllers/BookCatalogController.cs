﻿using Microsoft.AspNetCore.Mvc;
using Shared;
using Shared.Helpers;
using Shared.Models;
using System;
using System.Collections.Generic;
using NSwag.Annotations;

namespace WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BookCatalogController : ControllerBase
	{
		private readonly IBookService _bookService;

		public BookCatalogController(IBookService bookService)
		{
			_bookService = bookService;
		}

		/// <summary>
		/// Get Books List
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("books")]
		[ResponseType(typeof(IEnumerable<BookResponse>))]
		public ActionResult<IEnumerable<BookResponse>> GetBooks()
		{
			try
			{
				var books = _bookService.GetBooks();
				return books == null ? NotFound() : (ActionResult<IEnumerable<BookResponse>>) Ok(books);
			}
			catch (Exception)
			{
				return BadRequest();
			}
		}

		/// <summary>
		/// Get Book By Id
		/// </summary>
		/// <param name="id">identifier</param>
		/// <returns></returns>
		[HttpGet]
		[Route("book/{id}")]
		[ResponseType(typeof(BookResponse))]
		public ActionResult<BookResponse> GetBook(long id)
		{
			if (id <= 0)
			{
				return BadRequest();
			}

			try
			{
				var book = _bookService.GetBook(id);
				if (book == null)
				{
					return NotFound();
				}

				return Ok(book);
			}
			catch (Exception)
			{
				return BadRequest();
			}
		}

		/// <summary>
		/// Add new Book
		/// </summary>
		/// <param name="data">object for add new Book</param>
		/// <returns></returns>
		[HttpPost]
		[Route("book")]
		public ActionResult CreateBook([FromBody] BookRequest data)
		{
			if (!ValidationHelper.IsCreateValid(data, out var error))
			{
				return BadRequest(error);
			}

			var result = _bookService.CreateBook(data);
			if (result <= 0)
			{
				return BadRequest();
			}

			return Ok(result);
		}

		/// <summary>
		/// Update Book
		/// </summary>
		/// <param name="data">object for update existing Book</param>
		/// <returns></returns>
		[HttpPut]
		[Route("book")]
		public ActionResult UpdateBook([FromBody] BookRequest data)
		{
			if (!ValidationHelper.IsUpdateValid(data, out var error))
			{
				return BadRequest(error);
			}

			var result = _bookService.UpdateBook(data);
			if (!result)
			{
				return BadRequest();
			}

			return Ok();
		}

		/// <summary>
		/// Delete Book by id
		/// </summary>
		/// <param name="id">identifier</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("book/{id}")]
		public ActionResult DeleteBook(long id)
		{
			var result = _bookService.DeleteBook(id);
			if (!result)
			{
				return BadRequest();
			}

			return Ok();
		}
	}
}