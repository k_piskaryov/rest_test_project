﻿using Shared.DtoModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	/// <summary>
	/// Generic repository interface 
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IRepository<TEntity> where TEntity : IEntity
	{
		/// <summary>
		/// Create new entity in DB
		/// </summary>
		/// <param name="entity"></param>
		void Create(TEntity entity);

		/// <summary>
		/// Delete entity from DB by entity model
		/// </summary>
		/// <param name="entity"></param>
		void Delete(TEntity entity);

		/// <summary>
		/// Delete entity from DB by id
		/// </summary>
		/// <param name="id"></param>
		void Delete(long id);

		/// <summary>
		/// Update entity in DB
		/// </summary>
		/// <param name="entity"></param>
		void Update(TEntity entity);

		/// <summary>
		/// Get List Entities
		/// </summary>
		/// <returns></returns>
		IEnumerable<TEntity> Get();

		/// <summary>
		/// Get Entity by ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		TEntity GetById(long id);
	}
}
