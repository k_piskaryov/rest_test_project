﻿using Shared.DtoModels;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	/// <summary>
	/// interface service for CRUD operation with Book
	/// </summary>
	public interface IBookService
	{
		/// <summary>
		/// Create new Book in DB
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		long CreateBook(BookRequest data);

		/// <summary>
		/// Update Book in DB
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		bool UpdateBook(BookRequest data);

		/// <summary>
		/// Get List Books
		/// </summary>
		/// <returns></returns>
		IEnumerable<BookResponse> GetBooks();

		/// <summary>
		/// Get Book by Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		BookResponse GetBook(long id);

		/// <summary>
		/// Delete Book by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		bool DeleteBook(long id);
	}
}
