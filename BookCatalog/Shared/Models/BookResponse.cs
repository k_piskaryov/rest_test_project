﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
	/// <summary>
	/// Book data
	/// </summary>
	public class BookResponse
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		public long BookId { get; set; }

		/// <summary>
		/// Book Name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Book's Author
		/// </summary>
		public string Author { get; set; }

		/// <summary>
		/// Number of pages
		/// </summary>
		public int Pages { get; set; }
	}
}
