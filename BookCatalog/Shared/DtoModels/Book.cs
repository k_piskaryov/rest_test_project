﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.DtoModels
{
	/// <summary>
	/// Model Book in DataBase
	/// </summary>
	public class Book: IEntity
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Book Name
		/// </summary>
		[Required]
		public string Name { get; set; }

		/// <summary>
		/// Book's Author
		/// </summary>
		[Required]
		public string Author { get; set; }

		/// <summary>
		/// Number of pages
		/// </summary>
		[Required]
		public int Pages { get; set; }
	}
}
