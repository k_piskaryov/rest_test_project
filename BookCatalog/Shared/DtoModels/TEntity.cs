﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Shared.DtoModels
{
	/// <summary>
	/// Generic Model in DB 
	/// </summary>
	public interface IEntity
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		long Id { get; set; }
	}
}
