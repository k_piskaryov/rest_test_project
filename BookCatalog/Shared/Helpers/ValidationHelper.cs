﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Helpers
{
	/// <summary>
	/// Class Helper for Validate Request parameters
	/// </summary>
	public static class ValidationHelper
	{
		private const string ErrorNullOrEmptyFormat = "Field '{0}' not be null or empty; ";

		private const string ErrorLessOrEqualsZeroFormat = "Field '{0}' not be less or equals zero; ";

		/// <summary>
		/// Validate for Create new Book
		/// </summary>
		/// <param name="data"></param>
		/// <param name="error"></param>
		/// <returns>Is Valid result</returns>
		public static bool IsCreateValid(BookRequest data, out string error)
		{
			error = GetDefaultValidationErrors(data);

			return string.IsNullOrWhiteSpace(error);
		}

		/// <summary>
		/// Validate for Update Book
		/// </summary>
		/// <param name="data"></param>
		/// <param name="error"></param>
		/// <returns>Is Valid result</returns>
		public static bool IsUpdateValid(BookRequest data, out string error)
		{
			error = GetDefaultValidationErrors(data);
			if (data.BookId == null || data.BookId <= 0)
			{
				error += string.Format(ErrorLessOrEqualsZeroFormat, nameof(data.BookId));
			}
			return string.IsNullOrWhiteSpace(error);
		}

		private static string GetDefaultValidationErrors(BookRequest data)
		{
			var error = string.Empty;
			if (string.IsNullOrWhiteSpace(data.Name))
			{
				error += string.Format(ErrorNullOrEmptyFormat, nameof(data.Name));
			}
			if (string.IsNullOrWhiteSpace(data.Author))
			{
				error += string.Format(ErrorNullOrEmptyFormat, nameof(data.Author));
			}
			if (data.Pages <= 0)
			{
				error += string.Format(ErrorLessOrEqualsZeroFormat, nameof(data.Pages));
			}
			return error;
		}
	}
}
