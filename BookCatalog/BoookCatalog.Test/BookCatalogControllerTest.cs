using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Moq;
using NUnit.Framework;
using Shared;
using Shared.Models;
using WebApi.Controllers;

namespace BoookCatalog.Test
{
	public class BookCatalogControllerTest
	{
		private const int ValidId = 1;
		private const int InValidId = 100;

		private static readonly object[] InvalidCreateRequestTestCases =
		{
			new object[] {new BookRequest {Author = string.Empty, Name = string.Empty, Pages = 0}},
			new object[] {new BookRequest {Author = "Test", Name = string.Empty, Pages = 0}},
			new object[] {new BookRequest {Author = string.Empty, Name = "Test", Pages = 0}},
			new object[] {new BookRequest {Author = string.Empty, Name = string.Empty, Pages = 100}},
			new object[] {new BookRequest {Author = "Test", Name = "Test", Pages = 0}}
		};

		private static readonly object[] InvalidUpdateRequestTestCases =
		{
			new object[] {new BookRequest {BookId = -1, Author = string.Empty, Name = string.Empty, Pages = 0}},
			new object[] {new BookRequest {BookId = 0, Author = "Test", Name = string.Empty, Pages = 0}},
			new object[] {new BookRequest {BookId = 10, Author = string.Empty, Name = "Test", Pages = 0}},
			new object[] {new BookRequest {BookId = 10, Author = string.Empty, Name = string.Empty, Pages = 100}},
			new object[] {new BookRequest {BookId = 10, Author = "Test", Name = "Test", Pages = 0}}
		};

		private BookResponse _testBookItem;
		private BookRequest _testBookRequest;
		private BookRequest _testUpdateBookRequest;
		private IEnumerable<BookResponse> _testBooksList;
		private Mock<IBookService> _mockBookService;
		private BookCatalogController _controller;

		[SetUp]
		public void Setup()
		{
			InitializeParameters();
			InitializeMockServices();
			_controller = new BookCatalogController(_mockBookService.Object);
		}

		[Test]
		public void BookCatalogController_GetBooks_ReturnBooks()
		{
			var actionResult = _controller.GetBooks();

			var result = actionResult.Result as OkObjectResult;
			var valuesResult = (IEnumerable<BookResponse>)result?.Value;

			Assert.IsNotNull(result);
			Assert.IsInstanceOf(typeof(IEnumerable<BookResponse>), result.Value);
			Assert.True(valuesResult.Any());
		}

		[Test]
		public void BookCatalogController_GetBooks_ReturnNotFound()
		{
			var mockBookService = new Mock<IBookService>();
			mockBookService.Setup(x => x.GetBooks()).Returns((IEnumerable<BookResponse>) null);
			var controller = new BookCatalogController(mockBookService.Object);

			var actionResult = controller.GetBooks();
			var result = actionResult.Result as NotFoundResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_GetBook_InvalidId_BadRequest()
		{
			var actionResult = _controller.GetBook(InValidId);

			var result = actionResult.Result as NotFoundResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_GetBook_ValidId_ReturnBooks()
		{
			var actionResult = _controller.GetBook(ValidId);

			var result = actionResult.Result as OkObjectResult;
			var valueResult = (BookResponse)result?.Value;

			Assert.IsNotNull(result);
			Assert.IsInstanceOf(typeof(BookResponse), result.Value);
			Assert.IsNotNull(valueResult);
			Assert.IsNotEmpty(valueResult.Name);
			Assert.IsNotEmpty(valueResult.Author);
			Assert.AreEqual(valueResult.BookId, ValidId);
			Assert.True(valueResult.Pages > 0);
		}

		[Test]
		public void BookCatalogController_CreateBook_ValidDataServiceReturnBadResult_BadRequest()
		{
			var testRequest = new BookRequest
			{
				Name = "Tom",
				Author = "Ivan",
				Pages = 100
			};
			var actionResult = _controller.CreateBook(testRequest);
			var result = actionResult as BadRequestResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_CreateBook_ValidData_ReturnId()
		{
			var actionResult = _controller.CreateBook(_testBookRequest);
			var result = actionResult as OkObjectResult;

			Assert.IsNotNull(result);
			Assert.IsInstanceOf(typeof(long), result.Value);
			Assert.AreEqual((long) result.Value, ValidId);
		}

		[Test]
		public void BookCatalogController_DeleteBook_ValidId_Ok()
		{
			var actionResult = _controller.DeleteBook(ValidId);
			var result = actionResult as OkResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_DeleteBook_InvalidId_BadRequest()
		{
			var actionResult = _controller.DeleteBook(InValidId);
			var result = actionResult as BadRequestResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_UpdateBook_ServiceReturnFalse_BadRequest()
		{
			var testRequest = new BookRequest
			{
				BookId = InValidId,
				Author = "TestUpdateAuthor",
				Name = "TestUpdateName",
				Pages = 300
			};
			var actionResult = _controller.UpdateBook(testRequest);
			var result = actionResult as BadRequestResult;

			Assert.IsNotNull(result);
		}

		[Test]
		public void BookCatalogController_UpdateBook_ValidRequest_ReturnOk()
		{
			var actionResult = _controller.UpdateBook(_testUpdateBookRequest);
			var result = actionResult as OkResult;

			Assert.IsNotNull(result);
		}

		[Test, TestCaseSource("InvalidUpdateRequestTestCases")]
		public void BookCatalogController_UpdateBook_InValidRequest_BadRequestWithError(BookRequest request)
		{
			var actionResult = _controller.UpdateBook(request);
			var result = actionResult as BadRequestObjectResult;

			Assert.IsNotNull(result);
			Assert.IsInstanceOf(typeof(string), result.Value);
			Assert.IsNotEmpty((string)result.Value);
		}

		[Test, TestCaseSource("InvalidCreateRequestTestCases")]
		public void BookCatalogController_CreateBook_InValidRequest_BadRequestWithError(BookRequest request)
		{
			var actionResult = _controller.CreateBook(request);
			var result = actionResult as BadRequestObjectResult;

			Assert.IsNotNull(result);
			Assert.IsInstanceOf(typeof(string), result.Value);
			Assert.IsNotEmpty((string) result.Value);
		}

		private void InitializeParameters()
		{
			_testBookRequest = new BookRequest
			{
				Author = "TestAuthor",
				Name = "TestName",
				Pages = 100
			};

			_testUpdateBookRequest = new BookRequest
			{
				BookId = ValidId,
				Author = "TestUpdateAuthor",
				Name = "TestUpdateName",
				Pages = 300
			};

			_testBookItem = new BookResponse
			{
				BookId = ValidId,
				Author = "TestAuthor",
				Name = "TestName",
				Pages = 300
			};

			_testBooksList = GetBooksResponseList();
		}

		private void InitializeMockServices()
		{
			_mockBookService = new Mock<IBookService>();
			_mockBookService.Setup(x => x.CreateBook(_testBookRequest)).Returns(ValidId);
			_mockBookService.Setup(x => x.UpdateBook(_testUpdateBookRequest)).Returns(true);
			_mockBookService.Setup(x => x.GetBooks()).Returns(_testBooksList);
			_mockBookService.Setup(x => x.GetBook(ValidId)).Returns(_testBookItem);
			_mockBookService.Setup(x => x.DeleteBook(ValidId)).Returns(true);
		}

		private IEnumerable<BookResponse> GetBooksResponseList()
		{
			return new List<BookResponse>
			{
				new BookResponse
				{
					BookId = 1,
					Author = "TestAuthor1",
					Name = "TestName1",
					Pages = 100
				},
				new BookResponse
				{
					BookId = 2,
					Author = "TestAuthor2",
					Name = "TestName2",
					Pages = 200
				},
				new BookResponse
				{
					BookId = 3,
					Author = "TestAuthor3",
					Name = "TestName3",
					Pages = 300
				}
			};
		}
	}
}