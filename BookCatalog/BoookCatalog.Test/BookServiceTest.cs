﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBase;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Shared;
using Shared.Models;
using WebApi.Controllers;
using WebApi.Services;
using ILogger = Castle.Core.Logging.ILogger;

namespace BoookCatalog.Test
{
	public class BookServiceTest
	{
		private BookRepository _bookRepository;
		private BookCatalogContext _bookCatalogContext;
		private IBookService _bookService;

		[SetUp]
		public void Setup()
		{
			var mockLogger = new Mock<ILogger<BookService>>();
			var options = new DbContextOptionsBuilder<BookCatalogContext>()
				.UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
			_bookCatalogContext = new BookCatalogContext(options);
			_bookRepository = new BookRepository(_bookCatalogContext);
			_bookService = new BookService(_bookRepository, mockLogger.Object);

		}

		[Test]
		public void BookService_CreateBook_ValidRequest_ReturnId()
		{
			var testRequest = new BookRequest
			{
				Author = "TestIvan",
				Name = "BookTest",
				Pages = 1200
			};

			var result = _bookService.CreateBook(testRequest);

			Assert.True(result > 0);
		}

		[Test]
		public void BookService_UpdateBook_ValidRequest_UpdatedItem()
		{
			var testRequest = new BookRequest
			{
				Author = "Ivan",
				Name = "Books",
				Pages = 100
			};
			var testUpdateRequest = new BookRequest
			{
				Author = "Ivan1",
				Name = "Books1",
				Pages = 200
			};

			var createResult = _bookService.CreateBook(testRequest);
			testUpdateRequest.BookId = createResult;
			var updateResult = _bookService.UpdateBook(testUpdateRequest);
			var updatedItem = _bookService.GetBook(createResult);

			Assert.True(createResult > 0);
			Assert.True(updateResult);
			Assert.AreEqual(updatedItem.Author, testUpdateRequest.Author);
			Assert.AreEqual(updatedItem.Name, testUpdateRequest.Name);
			Assert.AreEqual(updatedItem.Pages, testUpdateRequest.Pages);
		}

		[Test]
		public void BookService_DeleteBook_DeletedItem()
		{
			var testRequest = new BookRequest
			{
				Author = "Kirill",
				Name = "BooksKirill",
				Pages = 1000
			};
			var createResult = _bookService.CreateBook(testRequest);
			var deleteResult = _bookService.DeleteBook(createResult);
			var deletedItem = _bookService.GetBook(createResult);

			Assert.True(createResult > 0);
			Assert.True(deleteResult);
			Assert.IsNull(deletedItem);
		}

		[Test]
		public void BookService_GetBook_GetItem()
		{
			var testRequest = new BookRequest
			{
				Author = "Pupkin",
				Name = "TErstAuthor",
				Pages = 700
			};
			var createResult = _bookService.CreateBook(testRequest);
			var item = _bookService.GetBook(createResult);

			Assert.True(createResult > 0);
			Assert.IsNotNull(item);
			Assert.AreEqual(item.BookId, createResult);
			Assert.AreEqual(item.Author, testRequest.Author);
			Assert.AreEqual(item.Name, testRequest.Name);
			Assert.AreEqual(item.Pages, testRequest.Pages);
		}

		[Test]
		public void BookService_GetBooks_GetItems()
		{
			var testRequest = new BookRequest
			{
				Author = "Ivan",
				Name = "Books",
				Pages = 100
			};
			var createResult = _bookService.CreateBook(testRequest);
			var items = _bookService.GetBooks();

			Assert.True(createResult > 0);
			Assert.IsNotNull(items);
			Assert.True(items.Count() == 1);
		}
	}
}
